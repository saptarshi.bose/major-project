<!DOCTYPE html>
<html lang="en">
<head>

     <title>Known - Education HTML Template</title>
<!-- 

Known Template 

https://templatemo.com/tm-516-known

-->
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/font-awesome.min.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">

     <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/templatemo-style.css">

</head>
<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>


     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="#" class="navbar-brand">Virtual Education</a>
               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-nav-first">
                        
                        
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                         <li><a href="#">Welcome To Virtual Education System</a></li>
                    </ul>
               </div>

          </div>
     </section>


     <!-- HOME -->
     <section id="home">
          <div class="row">

                    <div class="owl-carousel owl-theme home-slider">
                         <div class="item item-first">
                              <div class="caption">
                                   <div class="container">
                                        <div class="col-md-6 col-sm-12">
                                             
                                             
                                        </div>
                                   </div>
                              </div>
                         </div>

                         <div class="item item-second">
                              <div class="caption">
                                   <div class="container">
                                        <div class="col-md-6 col-sm-12">
                                             <h1>Start your journey with our practical courses</h1>
                                             <h3>Our online courses are built in partnership with technology leaders and are designed to meet industry demands.</h3>
                                             
                                        </div>
                                   </div>
                              </div>
                         </div>

                         <div class="item item-third">
                              <div class="caption">
                                   <div class="container">
                                        <div class="col-md-6 col-sm-12">
                                             <h1>Distance Learning Education Center</h1>
                                             <h3>Our online courses are designed to fit in your industry supporting all-round with latest technologies. <a rel="nofollow" href="https://www.facebook.com/templatemo">templatemo</a> page.</h3>
                                             
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
          </div>
     </section>


     <!-- FEATURE -->
     <section id="feature">
          <div class="container">
               <div class="row">

                    

                    <div class="col-md-4 col-sm-4 col-lg-offset-4">
                         <div class="feature-thumb">
                                   <ul class="social1-icon">
                                   <li><a href="" class="fa fa-lock" attr="facebook icon"></a></li>  
                                   </ul>
                              <h3>Login</h3>
                              <button type="button" class="btn btn-primary btn-block">Login For Student</button>
                              <button type="button" class="btn btn-primary btn-block">Login For Teacher</button>
                         </div>
                    </div>

                 


                    

               </div>
          </div>
     </section>


     

     


     <!-- FOOTER -->
     <footer id="footer">
        <div class="container">
             <div class="row">

                  <div class="col-md-4 col-sm-6">
                       <div class="footer-info">
                            <div class="section-title">
                                 <h2>Office</h2>
                            </div>
                            <address>
                                 <p>Salt Lake sector 5,<br> Kolkata 700091</p>
                            </address>

                            <ul class="social-icon">
                                 <li><a href="#" class="fa fa-facebook-square" attr="facebook icon"></a></li>
                                 <li><a href="#" class="fa fa-twitter"></a></li>
                                 <li><a href="#" class="fa fa-instagram"></a></li>
                            </ul>

                            <div class="copyright-text"> 
                                 <p>Virtual Education System</p>
                                 
                                 <p>Design: TemplateMo</p>
                            </div>
                       </div>
                  </div>

                  <div class="col-md-4 col-sm-6">
                       <div class="footer-info">
                            <div class="section-title">
                                 <h2>Contact Info</h2>
                            </div>
                            <address>
                                 <p>1800008099</p>
                                 <p><a href="mailto:youremail.co">admin@localhost</a></p>
                            </address>

                            <div class="footer_menu">
                                 <h2>Quick Links</h2>
                                 <ul>
                                      <li><a href="#">Career</a></li>
                                      <li><a href="#">Investor</a></li>
                                      <li><a href="#">Terms & Conditions</a></li>
                                      <li><a href="#">Refund Policy</a></li>
                                 </ul>
                            </div>
                       </div>
                  </div>

                  
                  
             </div>
        </div>
   </footer>


   <!-- SCRIPTS -->
   <script src="js/jquery.js"></script>
   <script src="js/bootstrap.min.js"></script>
   <script src="js/owl.carousel.min.js"></script>
   <script src="js/smoothscroll.js"></script>
   <script src="js/custom.js"></script>
</body>
</html>
